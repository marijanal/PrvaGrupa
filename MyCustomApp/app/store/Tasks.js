Ext.define('MyApp.store.Tasks', {
    extend: 'Ext.data.Store',
    model: 'MyApp.model.Task',
    storeId:'tasks',

    alias: 'store.tasks',
    data: { issues: []},
/*
    proxy: {
        type: 'ajax',
        url: 'http://localhost:8080/projects',
        params: Ext.encode({"token" : localStorage.getItem('token')}),
        reader: {
            type: 'json',
            rootProperty: 'projects'
        }
    }, 

    proxy: {
        type: 'ajax',
        url: 'MyCustomApp/resources/tasks.json',
        reader: {
            type: 'json',
            rootProperty: 'issues',
        }
    },
*/
    autoSync: true,

    load: function () {

        var name = Ext.util.History.getToken().split('/')[1];
        name = name.replace(" ", "%20");

        // var adresa = 'http://192.168.0.15:8080/projects/' + name + '/tickets';
        var adresa = 'http://localhost:8080/projects/' + name + '/tickets';
        Ext.Ajax.request({

            defaultHeaders: {
                'Content-Type': 'application/json'
            },

            url: adresa,
//            url: 'http://localhost:1841/MyCustomApp/resources/tasks.json',

            scope : this,
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                this.loadData(obj);


            },

            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    }

});
