Ext.define('MyApp.store.Project', {
    extend: 'Ext.data.Store',
    model: 'MyApp.model.Project',
    storeId:'projects',

    alias: 'store.project',
    data: { projects: []},

    proxy: {
        type: 'ajax',
        method: 'GET',
        // url: 'http://192.168.0.15:8080/projects',
        url: 'http://localhost:8080/projects',
        params: Ext.encode({"token" : localStorage.getItem('token')}),
        reader: {
            type: 'json',
            rootProperty: 'projects'
        }
    },

    autoSync: true,

    load: function () {

        Ext.Ajax.request({

            defaultHeaders: {
                'Content-Type': 'application/json'
            },

            // url: 'http://192.168.0.15:8080/projects',
            url: 'http://localhost:8080/projects',
            method: 'GET',
            scope : this,
            //params: Ext.encode({"token" : localStorage.getItem('token')}),

            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                this.loadData(obj);
            },

            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    }

});
