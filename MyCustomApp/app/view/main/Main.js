/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 */
Ext.define('MyApp.view.main.Main', {
    extend: 'Ext.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',
        'Ext.layout.Fit',
        'MyApp.view.register.Register',
        'MyApp.view.login.Login',
        'MyApp.view.dashboard.Dashboard',
        'MyApp.view.newTask.newTask',
        'MyApp.view.NotFound.NotFound'
    ],

    reference: 'main',
    controller: 'main',
    viewModel: 'main',

    layout: {
        type: 'vbox'
    },

    items: [
        {
            xtype: 'login',
            reference: 'login',
            hidden: true,
            flex: 1
        },
        {
            xtype: 'register',
            reference: 'register',
            hidden: true,
            flex: 1
        },
        {
            xtype: 'dashboard',
            reference: 'dashboard',
            hidden: true,
            flex: 1
        },
        {
            xtype: 'not-found',
            reference: 'not-found',
            hidden: true,
            flex: 1
        },
        {
            xtype: 'project',
            reference: 'project',
            hidden: true,
            flex: 1
        },
        {
            xtype: 'newTask',
            reference: 'newTask',
            hidden: true,
            flex: 1
        },
        {
            xtype: 'editTask',
            reference: 'editTask',
            hidden: true,
            flex: 1
        },
        {
            xtype: 'viewTask',
            reference: 'viewTask',
            hidden: true,
            flex: 1

        },
        {
            xtype: 'infoPage',
            reference: 'infoPage',
            hidden: true,
            flex: 1
        }
    ]
});
