/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('MyApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    requires: [
        'MyApp.store.AllUsers',
    ],

    routes: {

        'login': {

            action:
                function () {
                    var isLoggedIn = localStorage.getItem('token');

                    if (isLoggedIn) {
                        this.redirectTo('dashboard', false);
                    }
                    this.hideAllExceptThis('login');
                }
        },

        'register': {

            action: function () {
                var isLoggedIn = localStorage.getItem('token');

                if (isLoggedIn) {
                    this.redirectTo('dashboard', false);
                }
                this.hideAllExceptThis('register');
            }
        },

        'dashboard': {

            action: function () {

                if (localStorage.getItem('token'))
                    Ext.getStore('projects').load();

                this.hideAllExceptThis('dashboard');


            }
        },

        'dashboard/:name': {

            action: function () {
                this.hideAllExceptThis('project');
//                if (localStorage.getItem('token'))
                Ext.getStore('tasks').load();
            }
        },

        'dashboard/:name/newTask': {
            action: function () {

                this.hideAllExceptThis('newTask');

            }
        },

        'dashboard/:name/task/:tid': {

            action: function () {

                var taskView = Ext.getCmp('viewTask');

                var taskId = Ext.util.History.getToken().split('/')[3];
                Ext.Ajax.request({

                    defaultHeaders: {
                        'Content-Type': 'application/json'
                    },
                    scope: this,

                    method: 'GET',
                    // url: 'http://192.168.0.15:8080/tickets/' + taskId,
                    url: 'http://localhost:8080/tickets/' + taskId,


                    success: function (response, opts) {
                        var rspn = Ext.decode(response.responseText);
                        rspn.ticketType = rspn.ticketType.replace("_"," ");
                        rspn.ticketType = rspn.ticketType.toLowerCase();
                        rspn.ticketType = Ext.String.capitalize(rspn.ticketType);
                        console.log(rspn);
                         taskView.setValues(rspn);

                        this.hideAllExceptThis('viewTask');
                    },

                    failure: function (response, opts) {

                    }
                });
            }
        },
        'infoPage': {

            action: function () {
                this.hideAllExceptThis('infoPage');
            }
        }
/*
        '*': {

            action: function () {

                this.hideAllExceptThis('not-found');
            }
        }
*/
    },

    init: function () {
        this.getView().setActiveItem(0);
    },

    hideAllExceptThis: function (xtype) {

        if (xtype === 'dashboard' && (localStorage.getItem('token') == null))
            xtype = 'login';

        this.getView().getActiveItem().hide();
        this.getView().setActiveItem(xtype);
        this.getView().lookup(xtype).show();

    }
});
