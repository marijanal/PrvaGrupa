Ext.define('MyApp.view.newTask.newTaskModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.newTask',

    data: localStorage.getItem("language") === 'rs' ? {

        appLocale: {
            assignee: "Заступник",
            btnBack: "Назад",
            btnAdd: "Потврди",
            description: "Опис",
            storyPoints: "Поени",
            summary: "Резиме",
            title: "Нови задатак",
            tip: "Тип",
            statusLabel: 'Статус',
            type: [{
                text: 'Задатак',
                value: 'task'
            }, {
                text: 'Квар',
                value: 'bug'
            }, {
                text: 'Одлика',
                value: 'new_feature'
            }, {
                text: 'Побољшање',
                value: 'improvement'
            }],
            status: [{
                text: 'За урадити',
                value: 'TO_DO'
            },{
                text: 'Решавање у току',
                value: 'IN_PROGRESS'
            },{
                text: 'Решено',
                value: 'RESOLVED'
            }]
        }

    } : {

        appLocale:{
            assignee: "Assignee",
            btnBack: "Back",
            btnAdd: "Submit",
            description: "Description",
            storyPoints: "Points",
            summary: "Summary",
            title: "New task",
            tip: "Type",
            statusLabel: 'Status',
            type: [{
                text: 'Task',
                value: 'task'
            }, {
                text: 'Bug',
                value: 'bug'
            }, {
                text: 'Feature',
                value: 'new_feature'
            }, {
                text: 'Improvement',
                value: 'improvement'
            }],
            status: [{
                text: 'To do',
                value: 'TO_DO'
            },{
                text: 'In progress',
                value: 'IN_PROGRESS'
            },{
                text: 'Resolved',
                value: 'RESOLVED'
            }]
        }
    }
});
