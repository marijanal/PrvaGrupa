var username = localStorage.getItem('token');
Ext.define('MyApp.view.newTask.newTask', {
    extend: 'Ext.form.Panel',
    xtype: 'newTask',

    controller: 'newTask',
    viewModel: 'newTask',
    bind: {title: '{appLocale.title}'},

    layout: {

        type: 'vbox'
    },

    requires: [
        'MyApp.view.dashboard.DashboardController'
    ],

    // {
    //     "description": "string",
    //     "points": 0,
    //     "project_id": 0,
    //     "resolvedDate": "2018-08-15",
    //     "status": "TO_DO",
    //     "summary": "string",
    //     "ticketType": "BUG",
    //     "updatedDate": "2018-08-15",
    //     "username": "string"
    // }

    items: [
    //     {
    //     xtype: 'combobox',
    //     id: 'userSlct',
    //     bind: {
    //         label: '{appLocale.assignee}'
    //     },
    //     autoSelect: true,
    //     required: true
    // },
        {
            xtype: 'textfield',
            bind: {label: '{appLocale.description}'},
            name: 'description',
            required: true
        }
        ,
        {
            xtype: 'numberfield',
            bind: {label: '{appLocale.storyPoints}'},
            name: 'points',
            allowBlank: false,
            required: true
        }
        ,
        {
            xtype: 'textfield',
            reference: 'newTaskSummary',
            bind: {label: '{appLocale.summary}'},
            name: 'summary',
            required: true
        }
        ,
        {
            xtype: 'combobox',
            reference: 'type',
            bind: {
                options: '{appLocale.type}',
                label: '{appLocale.tip}'
            },
            autoSelect: true,
            name: 'ticketType',
            required: true
        },
        {
            xtype: 'combobox',
            reference: 'status',
            bind: {
                options: '{appLocale.status}',
                label: '{appLocale.statusLabel}'
            },
            autoSelect: true,
            name: 'status',
            required: true
        },
        {
            xtype: 'button',
            ui: 'squareBtn',
            bind: {text: '{appLocale.btnAdd}'},
            handler: 'onSubmit'
        }

    ],


    buttons: [{
        iconCls: 'x-fa fa-angle-left',
        scale: 'huge',
        handler: 'goBack'
    }, {
        xtype: 'spacer'
    }, {
        iconCls: 'x-fa fa-home',
        scale: 'huge',
        handler: 'goHome'
    }],
    tools: [
        {
            iconCls: 'x-fa fa-info',
            handler: 'goInfoPage'
        }, {
            iconCls: 'x-fa fa-sign-out',
            handler: 'onSignOut'
        }]

});
