var projectName;
var projectId;
var win;

Ext.define('MyApp.view.newTask.newTaskController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.newTask',

    listen: {
        controller: {
            'dashboard' : {
                'getProject': function (name, id) {
                    projectName = name;
                    projectId = id;
                }
            }
        }
    },

    goInfoPage: function () {
        this.redirectTo('infoPage');
    },

    close: function () {
        win.close();
    },

    goBack: function () {
        window.history.back();
    },

    goHome: function () {
        this.redirectTo('dashboard');
    },

    onSubmit: function () {


         var name = Ext.util.History.getToken().split('/')[1];  // ime projekta

        var form = this.getView().getValues();

        this.getView().validate();

        if(this.getView().isValid()) {
            form.project_id = projectId;
            var today = new Date();
            form.resolvedDate = Ext.Date.format(today, "Y-m-d");
            form.updatedDate = Ext.Date.format(today, "Y-m-d");
            form.username = localStorage.getItem('token');
            form.ticketType = form.ticketType.toUpperCase();

            Ext.Ajax.request({

                defaultHeaders: {
                    'Content-Type': 'application/json'
                },

                // url: 'http://192.168.0.15:8080/tickets',
                url: 'http://localhost:8080/tickets',
                method: 'POST',
                scope: this,
                params: Ext.encode(form),

                success: function (response, opts) {
                    var obj = Ext.decode(response.responseText);
                    if (obj) {
                        this.getView().reset(true);
                        this.redirectTo("dashboard/" + name);
                    }
                    else {
                        Ext.Msg.alert('Notification', 'Error.');
                    }
                },

                failure: function (response, opts) {

                    Ext.Msg.alert('Notification', 'Error.');
                }
            });
        }
        else {
            win = Ext.create({
                xtype: 'dialog',
                draggable: false,
                viewModel: 'register',
                controller: 'register',
                bind: {
                    title: '{appLocale.form}',
                    html: '{appLocale.formFail}'
                },

                buttons: [{
                    bind: {text: '{appLocale.okBtn}'},
                    handler: 'closeDialog'
                }]
            }).show();
        }
    },


    onSignOut: function () {

        win = Ext.create("Ext.window.Window", {
            controller: 'dashboard',
            viewModel: 'dashboard',
            draggable: false,
            bind: {title: '{appLocale.signOut}'},

            items: [{
                xtype: 'panel',
                bind: {html: '{appLocale.question}'}
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [{
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-check',
                    bind: {text: '{appLocale.yes}'},
                    handler: 'signOut'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-times',
                    bind: {text: '{appLocale.no}'},
                    handler: 'close'
                }]
            }]


        }).show();
    },

    signOut: function () {
        localStorage.removeItem("token");
        window.location.reload();
    }
});
