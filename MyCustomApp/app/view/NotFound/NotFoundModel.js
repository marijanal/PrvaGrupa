/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('MyApp.view.NotFound.NotFoundModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.notFound',

    data: localStorage.getItem("language") === 'rs' ? {
        appLocale: {

            btnBack: "Назад",
            text: "<h1>404 <br> <br> Страница не постоји</h1>"

        }

    } : {

        appLocale: {

            btnBack: "Back",
            text: "<h1>404 <br> <br> Page not found</h1>"

        }
    }

});
