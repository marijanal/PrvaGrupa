/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 */
Ext.define('MyApp.view.NotFound.NotFound', {
    extend: 'Ext.container.Container',
    xtype: 'not-found',
    controller: 'notFound',
    viewModel: 'notFound',

    requires: [
        'Ext.MessageBox',
        'Ext.layout.Fit',
        'MyApp.view.NotFound.NotFoundController'
    ],

    reference: 'not-found',

    layout: {
        type: 'vbox',
        align: 'middle'
    },
    bind:{html:'{appLocale.text}'},
    items: [
        {
            xtype:'button',
            bind: {text:'{appLocale.btnBack}'},
            handler: 'onBack'

        }
    ]
});
