/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('MyApp.view.NotFound.NotFoundController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.notFound',

    onBack: function () {

        var token = localStorage.getItem('token');

        if(token == null)
        {
            this.redirectTo('login', false);
        }
        else
            this.redirectTo('dashboard', false);
    }


});
