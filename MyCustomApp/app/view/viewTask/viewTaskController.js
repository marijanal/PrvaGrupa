Ext.define('MyApp.view.viewTask.viewTaskController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.viewTask',


    listen: {
        controller: {
            'project': {
                'setValuesOfTask': function (taskData) {
                    console.log(taskData);
                    this.getView().setValues(taskData);
                }
            }
        }

    },
    onSubmit: function () {

        var id = Ext.util.History.getToken().split('/')[3];
        var form = this.getView().getValues();

        form.username = localStorage.getItem('token');
        form.ticketType = form.ticketType.toUpperCase();
        form.id = id;


        var name = Ext.util.History.getToken().split('/')[1];

        Ext.Ajax.request({

            defaultHeaders: {
                'Content-Type': 'application/json'
            },

            // url: 'http://192.168.0.15:8080/tickets',
            url: 'http://localhost:8080/tickets',
            method: 'PUT',
            scope: this,
            params: Ext.encode(form),

            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                if(obj) {
                    this.redirectTo("dashboard/" + name);
                }
                else {
                    Ext.Msg.alert('Notification', 'Error.');
                }
            },

            failure: function (response, opts) {

                Ext.Msg.alert('Notification', 'Error.');
            }
        });

    },


    goBack: function () {
        window.history.back();
    },

    goHome: function () {
        this.redirectTo('dashboard');
    },
    goInfoPage: function () {
        this.redirectTo('infoPage');
    },
    onSignOut: function () {

        win = Ext.create("Ext.window.Window", {
            controller: 'dashboard',
            viewModel: 'dashboard',
            draggable: false,
            bind: {title: '{appLocale.signOut}'},

            items: [{
                xtype: 'panel',
                bind: {html: '{appLocale.question}'}
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [{
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-check',
                    bind: {text: '{appLocale.yes}'},
                    handler: 'signOut'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-times',
                    bind: {text: '{appLocale.no}'},
                    handler: 'close'
                }]
            }]

        }).show();
    },

    signOut: function () {
        localStorage.removeItem("token");
        window.location.reload();
    }

});