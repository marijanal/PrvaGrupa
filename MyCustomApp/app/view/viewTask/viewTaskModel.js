Ext.define('MyApp.view.newTask.viewTaskModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.viewTask',

    data: localStorage.getItem("language") === 'rs' ? {

        appLocale: {
            assignee: "Заступник",
            btnBack: "Назад",
            title: "Измени задатак"

        }

    } : {

        appLocale:{
            assignee: "Assignee",
            btnBack: "Back",
            btnAdd: "Submit",
            description: "Description",
            storyPoints: "Points",
            summary: "Summary",
            title: "Edit task",
            tip: "Type",
            statusLabel: 'Status',
            type: [{
                text: 'Task',
                value: 'task'
            }, {
                text: 'Bug',
                value: 'bug'
            }, {
                text: 'Feature',
                value: 'new_feature'
            }, {
                text: 'Improvement',
                value: 'improvement'
            }],
            status: [{
                text: 'To do',
                value: 'TO_DO'
            },{
                text: 'In progress',
                value: 'IN_PROGRESS'
            },{
                text: 'Resolved',
                value: 'RESOLVED'
            }]
        }
    }
});
