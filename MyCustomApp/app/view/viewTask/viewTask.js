Ext.define('MyApp.view.viewTask.viewTask', {
    extend: 'Ext.form.Panel',
    xtype: 'viewTask',


    id: 'viewTask',

    controller: 'viewTask',
    viewModel: 'viewTask',
    bind: {title: '{appLocale.title}'},

    layout: {
        type: 'vbox',
    },
    items: [
        {
            xtype: 'textfield',
            bind: {label: '{appLocale.description}'},
            name: 'description',
            required: true
        }
        ,
        {
            xtype: 'numberfield',
            bind: {label: '{appLocale.storyPoints}'},
            name: 'points',
            allowBlank: false,
            required: true
        }
        ,
        {
            xtype: 'textfield',
            reference: 'newTaskSummary',
            bind: {label: '{appLocale.summary}'},
            name: 'summary',
            required: true
        }
        ,
        {
            xtype: 'combobox',
            reference: 'type',
            bind: {
                options: '{appLocale.type}',
                label: '{appLocale.tip}'
            },
            autoSelect: true,
            name: 'ticketType',
            required: true
        },
        {
            xtype: 'combobox',
            reference: 'status',
            bind: {
                options: '{appLocale.status}',
                label: '{appLocale.statusLabel}'
            },
            autoSelect: true,
            name: 'status',
            required: true
        }

    ],

    buttons: [{
        iconCls: 'x-fa fa-angle-left',
        scale: 'huge',
        handler: 'goBack'
    },{
        xtype: 'spacer'
    },
        {
            xtype: 'button',
            bind: {text: '{appLocale.btnAdd}'},
            handler: 'onSubmit'
        },
        {
            xtype: 'spacer'
        }, {
            iconCls: 'x-fa fa-home',
            scale: 'huge',
            handler: 'goHome'
        }],
    tools: [
        {
            iconCls: 'x-fa fa-info',
            handler: 'goInfoPage'
        }, {
            iconCls: 'x-fa fa-sign-out',
            handler: 'onSignOut'
        }]
});