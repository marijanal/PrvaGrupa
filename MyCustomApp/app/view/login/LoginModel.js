Ext.define('MyApp.view.login.LoginModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.login',

    data: localStorage.getItem("language") === 'rs' ? {
        appLocale:{
            btnSubmit: "Потврди",
            btnRegister: "Региструј се",
            label: "<p>Немате налог?</p>",
            footer: "<footer>Copyright © 2018 by <a href='http://emisia.net' target='_blank'>Emisia</a> - Radionica UI</footer>",
            username: "Корисничко име",
            password: "Лозинка",
            title: 'Пријави се',
            loginFail: 'Грешка у пријави',
            loginFailMsg: 'Корисничко име или лозинка су неисправни',
            okBtn: 'У РЕДУ',
            formFail: 'Обавештење',
            formFailMsg: 'Попуните сва поља.'
        }

    } : {

        appLocale:{
            btnSubmit: "Submit",
            btnRegister: "Register",
            label: "<p>Don't have an account?</p>",
            footer: "<footer>Copyright © 2018 by <a href='http://emisia.net' target='_blank'>Emisia</a> - Radionica UI</footer>",
            username: "Username",
            password: "Password",
            title: 'Login',
            loginFail: 'Login fail',
            loginFailMsg: 'Username or password is wrong.',
            okBtn: 'OK',
            formFail: 'Notification',
            formFailMsg: 'Fill all fields.'
        }
    }

});