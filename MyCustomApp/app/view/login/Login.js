Ext.define('MyApp.view.login.Login', {
    extend: 'Ext.form.Panel',
    xtype: 'login',

    bind: {title: '{appLocale.title}'},
    controller: 'login',
    id: 'login',
    listener: 'login',
    viewModel: 'login',

    layout: {
        type: 'vbox',
        pack: 'middle'
    },

    items: [{
        xtype: 'textfield',
        reference: 'loginUser',
        bind: {label: '{appLocale.username}'},
        id: 'username',
        name: 'username',
        required: true
    }, {
        xtype: 'passwordfield',
        reference: 'passUser',
        bind: {label: '{appLocale.password}'},
        id: 'password',
        name: 'password',
        required: true
    },
        {
            xtype: 'spacer',
            width: 20
        },
        {
        xtype: 'button',
        bind: {text: '{appLocale.btnSubmit}'},
        id: 'btnSubmit',
        reference: 'submit',
        handler: 'onSubmit',
        ui: 'squareBtn'
    }, {
        xtype: 'panel',
        buttons: [{
            cls: 'registerArea',
            xtype: 'label',
            bind: {html: '{appLocale.label}'}
        }, {
            xtype: 'button',
            bind: {text: '{appLocale.btnRegister}'},
            id: 'btnRegister',
            handler: 'redirectOnRegister',
            ui: 'squareBtn'
        }]
    }, {
        xtype: 'panel',
        docked: 'bottom',
        bind: {html: '{appLocale.footer}'}
    }],

    tools: [{
        html: '<img src="resources/languageIcons/serbia.svg" width="32" height="32">',
        handler: 'onSerbianClick'
    }, {
        xtype: 'spacer',
        width: 10
    }, {
        html: '<img src="resources/languageIcons/united-kingdom.svg" width="32" height="32">',
        handler: 'onEnglishClick'
    },

        {
            iconCls: 'x-fa fa-info',
            handler: 'goInfoPage'
        }]

});