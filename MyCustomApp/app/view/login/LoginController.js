var win;
Ext.define('MyApp.view.login.LoginController', {

    alias: 'controller.login',
    extend: 'Ext.app.ViewController',


    goInfoPage: function () {
        this.redirectTo('infoPage');
    },

    onSubmit: function () {

        var loginForm = this.getView().getValues();
        var login = this.getView();

        this.getView().validate();

        if (!this.getView().isValid()) {
            win = Ext.create({
                xtype: 'dialog',
                draggable: false,
                viewModel: 'login',
                controller: 'register',
                bind: {
                    title: '{appLocale.formFail}',
                    html: '{appLocale.formFailMsg}'
                },

                buttons: [{
                    bind: {text: '{appLocale.okBtn}'},
                    handler: 'closeDialog'
                }]
            }).show();
        }
        else {
            Ext.Ajax.request({

                defaultHeaders: {
                    'Content-Type': 'application/json'
                },

                // url: 'http://192.168.0.15:8080/users/login',
                url: 'http://localhost:8080/users/login',
                scope: this,
                params: Ext.encode(loginForm),

                success: function (response, opts) {
                    //var obj = Ext.decode(response.responseText);
                    localStorage.setItem('token', loginForm.username);
                    this.redirectTo('dashboard', false);
                },

                failure: function (response, opts) {

                    login.lookup('loginUser').toggleInvalidCls(true);
                    login.lookup('passUser').toggleInvalidCls(true);
                    win = Ext.create({
                        xtype: 'dialog',
                        draggable: false,
                        viewModel: 'login',
                        controller: 'register',
                        bind: {
                            title: '{appLocale.loginFail}',
                            html: '{appLocale.loginFailMsg}'
                        },

                        buttons: [{
                            bind: {text: '{appLocale.okBtn}'},
                            handler: 'closeDialog'
                        }]
                    }).show();
                }
            });
        }
    },

    redirectOnRegister: function () {

        this.redirectTo('register', false);
    },

    onSerbianClick: function() {
        if(!(localStorage.getItem("language") === "rs")) {
            localStorage.setItem("language", "rs");
            window.location.reload();
        }
    },

    onEnglishClick: function() {
        if(!(localStorage.getItem("language") === "en" || localStorage.getItem("language") === "")) {
            localStorage.setItem("language", "en");
            window.location.reload();
        }
    }

});