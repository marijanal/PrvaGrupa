Ext.define('MyApp.view.infoPage.infoPage', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.layout.Card'
    ],
    xtype: 'infoPage',
    controller: 'infoPage',
    viewModel: 'infoPage',
    layout: 'card',

    bind: {title: '{appLocale.title}'},

    style: 'text-align: justify;',

    items: [
        {
            id: 'card-0',
            cls: 'infoPadding',
            bind: {html: '{appLocale.card0}'},
            scrollable: 'y'
        },
        {
            id: 'card-1',
            bind: {html: '{appLocale.card1}'},
            scrollable: 'y'
        }
    ],

    buttons: [
        {
            itemId: 'card-prev',
            cls: 'infoPadding',
            bind: {text: '{appLocale.prev}'},
            handler: 'showPrevious',
            disabled: true
        },
        {
            xtype: 'spacer'
        },
        {
            iconCls: 'x-fa fa-home',
            scale: 'huge',
            handler: 'goHome'
        },
        {
            xtype: 'spacer'
        },
        {
            itemId: 'card-next',
            bind: {text: '{appLocale.next}'},
            handler: 'showNext'
        }
    ]

});
