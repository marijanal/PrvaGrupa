var win;
Ext.define('MyApp.view.infoPage.infoPageController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.infoPage',

    init: function () {

        this.getView().setActiveItem(0);
    },

    close: function () {
        win.close();
    },

    doCardNavigation: function (incr) {
        var me = this.getView();
        var id = me.getActiveItem().id;
        var i = id.split('card-')[1];
        var next = parseInt(i, 10) + incr;
        me.setActiveItem(next);

        me.down('#card-prev').setDisabled(next === 0);
        me.down('#card-next').setDisabled(next === 1);
    },

    goBack: function () {
        window.history.back();
    },

    goInfoPage: function () {
        this.redirectTo('infoPage');
    },

    goHome: function () {
        this.redirectTo('login');
    },

    showNext: function () {
        this.doCardNavigation(1);
    },

    showPrevious: function () {
        this.doCardNavigation(-1);
    },


    onSignOut: function () {

        win = Ext.create("Ext.window.Window", {
            controller: 'dashboard',
            viewModel: 'dashboard',
            draggable: false,
            bind: {title: '{appLocale.signOut}'},

            items: [{
                xtype: 'panel',
                bind: {html: '{appLocale.question}'}
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [{
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-check',
                    bind: {text: '{appLocale.yes}'},
                    handler: 'signOut'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-times',
                    bind: {text: '{appLocale.no}'},
                    handler: 'close'
                }]
            }]


        }).show();
    },

    signOut: function () {
        localStorage.removeItem("token");
        window.location.reload();
    }

});
