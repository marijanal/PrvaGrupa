/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('MyApp.view.infoPage.infoPageModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.infoPage',

    data: localStorage.getItem("language") === 'rs' ? {

        appLocale: {
            next: "Следеће &raquo;",
            prev: "&laquo; Претходно",
            card0: "<p>Поштовани корисници, <br> у наставку овог текста биће описан начин употребе апликације за праћење таскова. " +
            "Почетна страна апликације је страна за пријаву где је на слици испод приказан пример. Уколико до сада нисте користили апликацију и немате налог морате да прво направити, регистровати. Налог представља податке корисника на једном месту. " +
            " Након успешне регистрације користи се корисничко име и лозинка приликом пријаве. У случају грешке приказаће Вам се одговарајућа порука. <br> Такође на почетку можете изабрати језик на коме желите да се апликација приказује. </p> " +
            "<img src=\"resources/images/login.png\"> <img src=\"http://localhost:1841/MyCustomApp/resources/images/register.png\" width=\"47%\" ><br>",
            card1: "<p>Након успешне пријаве приказује Вам се списак свих пројеката. Кликом на дугме \"НОВИ ПРОЈЕКАТ\" отвара се прозор где се уноси име новог пројекта након чега ће он бити приказан на листи.  </p>" +
            "<img src=\"resources/images/dashboard.png\" >" +
            "<img src=\"resources/images/noviProjekat.png\"> <br>" +
            "<p>Избором пројекта излистани су сви таскови тог пројекта, након чега можете креирати нови таск (дугме \"НОВИ ТАСК\" или приказати детаљнији приказ.</p> ",
            no: "Не",
            signOut: "Одјави се",
            question: "Да ли сте сигурни да желите да се одјавите?",
            yes: "Да",
            title: "Упутство"
        }

    } : {

        appLocale: {
            next: "Next &raquo;",
            prev: "&laquo; Previous",
            card0: "<p>Poštovani korisnici, <br> u nastavku ovog teksta biće opisan način upotrebe aplikacije za praćenje taskova. " +
            "Početna strana aplikacije je strana za prijavu gde je na slici ispod prikazan primer. Ukoliko do sada niste koristili aplikaciju i nemate nalog morate da prvo napraviti, registrovati. Nalog predstavlja podatke korisnika na jednom mestu. " +
            " Nakon uspešne registracije koristi se korisničko ime i lozinka prilikom prijave. U slučaju greške prikazaće Vam se odgovarajuća poruka. <br> Takođe na početku možete izabrati jezik na kome želite da se aplikacija prikazuje. </p> " +
            "<img src=\"resources/images/login.png\"> <img src=\"http://localhost:1841/MyCustomApp/resources/images/register.png\" width=\"47%\" ><br>",
            card1: "<p>Nakon uspešne prijave prikazuje Vam se spisak svih projekata. Klikom na dugme \"NOVI PROJEKAT\" otvara se prozor gde se unosi ime novog projekta nakon čega će on biti prikazan na listi.  </p>" +
            "<div class= \"infoImages\">"+
                "<img src=\"resources/images/dashboard.png\">" +
            "<img src=\"resources/images/noviProjekat.png\"> <br>" +
            "<div>"+
            "<p>Izborom projekta izlistani su svi taskovi tog projekta, nakon čega možete kreirati novi task (dugme \"NOVI TASK\" ili prikazati detaljniji prikaz.</p> ",
            title: "Info page",
            no: "No",
            projectName: "Project name",
            signOut: "Log out",
            titleNew: "New project",

            question: "Are you sure want to log out?",
            yes: "Yes"
        }
    }

});
