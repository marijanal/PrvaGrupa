Ext.define('MyApp.view.editTask.EditTaskModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.editTask',

    data: localStorage.getItem("language") === 'rs' ? {

        appLocale: {
            assignee: "Заступник",
            btnBack: "Назад",
            btnEdit: "Уреди",
            description: "Опис",
            storyPoints: "Приче",
            summary: "Резиме",
            title: "Нови задатак",
            tip: "Type",
            type: [{
                text: 'Задатак',
                value: 'task'
            }, {
                text: 'Квар',
                value: 'bug'
            }, {
                text: 'Одлика',
                value: 'feature'
            }, {
                text: 'Побољшање',
                value: 'improvement'
            }]
        },
        currentTask: {
            description: "опис",
            storyPoints: 0,
            summary: "string",
            type: "Задатак"
        }

    } : {

        appLocale: {
            assignee: "Assignee",
            btnBack: "Back",
            btnEdit: "Edit",
            description: "Description",
            storyPoints: "Story Points",
            summary: "Summary",
            title: "Edit task",
            tip: "Type",
            type: [{
                text: 'Task',
                value: 'task'
            }, {
                text: 'Bug',
                value: 'bug'
            }, {
                text: 'Feature',
                value: 'feature'
            }, {
                text: 'Improvement',
                value: 'improvement'
            }]
        },
        currentTask: {
            description: "description",
            storyPoints: 0,
            summary: "string",
            type: "Task"
        }
    }
});