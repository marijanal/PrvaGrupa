var win;

Ext.define('MyApp.view.editTask.EditTaskController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.editTask',
    listen: {
        controller: {
            'project': {
                'sendTaskData': function (data) {

                    this.getView().lookup('description').setHtml("<h2>Description: </h2><p>" + data.description + "</p>");
                    this.getView().lookup('storyPoints').setHtml("<h2>Story point: </h2><p>" + data.storyPoints + "</p>");
                    this.getView().lookup('summary').setHtml("<h2>Summary: </h2><p>" + data.summary + "</p>");
                    this.getView().lookup('type').setHtml("<h2>Type: </h2><p>" + data.type + "</p>");
                }
            }
        }
    },

    goInfoPage: function () {
        this.redirectTo('infoPage');
    },
    onSubmit: function(test){
        win = Ext.create("Ext.window.Window", {
            controller: 'editTask',
            viewModel: 'editTask',
            //bind:{title: '{appLocale.signOut}'},

            items: [{
                xtype: 'panel',
                //bind: {html: '{appLocale.question}'},
                html: 'edit'
            },  {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [{
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    bind: {text: '{appLocale.btnAdd}'},
                    handler: 'addProject',
                    iconCls: 'x-fa fa-file'
                }, {
                    xtype: 'button',
                    bind: {text: '{appLocale.btnClose}'},
                    iconCls: 'x-fa fa-times',
                    handler: 'close'
                }]
            }]


        }).show();
    },

    close: function () {
        win.close();
    },

    goBack: function () {
        window.history.back();
    },

    goHome: function () {
        this.redirectTo('dashboard');
    },

    onSignOut: function () {

        win = Ext.create("Ext.window.Window", {
            controller: 'dashboard',
            viewModel: 'dashboard',
            bind:{title: '{appLocale.signOut}'},

            items: [{
                xtype: 'panel',
                bind: {html: '{appLocale.question}'}
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [{
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-check',
                    bind:{text:'{appLocale.yes}'},
                    handler: 'signOut'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-times',
                    bind:{text:'{appLocale.no}'},
                    handler: 'close'
                }]
            }]


        }).show();
    },

    signOut: function () {
        localStorage.clear();
        window.location.reload();
    }

});