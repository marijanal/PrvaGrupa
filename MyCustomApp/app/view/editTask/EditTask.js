var username = localStorage.getItem('token');
Ext.define('MyApp.view.editTask.EditTask', {
    extend: 'Ext.panel.Panel',
    xtype: 'editTask',
    controller: 'editTask',
    viewModel: 'editTask',
    bind: {title: '{appLocale.title}'},

    layout: {

        type: 'vbox'
    },

    items: [{
        xtype: 'panel',
        layout: {
            type: 'hbox',
            pack: 'center'
        },
        items: [{
            xtype: 'label',
            reference: 'description',
            name: 'description',
            flex: 1
        }, {
            xtype: 'button',
            reference: 'descriptionSubmitBtn',
            // bind: {text: '{appLocale.btnEdit}'},
            iconCls: 'x-fa fa-pencil-square-o',
            handler: 'onSubmit',
            flex: 0.2
        }]
    }, {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            pack: 'center'
        },
        items: [{
            xtype: 'label',
            reference: 'storyPoints',
            name: 'storyPoints',
            flex: 1
        }, {
            xtype: 'button',
            reference: 'storyPointsBtn',
            handler: 'onSubmit',
            iconCls: 'x-fa fa-pencil-square-o',
            flex: 0.2

        }]
    }, {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            pack: 'center'
        },
        items: [{
            xtype: 'label',
            reference: 'summary',
            name: 'summary',
            flex: 1
        }, {
            xtype: 'button',
            reference: 'summaryBtn',
            handler: 'onSubmit',
            iconCls: 'x-fa fa-pencil-square-o',
            flex: 0.2

        }]
    }, {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            pack: 'center'
        },
        items: [{
            xtype: 'label',
            reference: 'type',
            name: 'type',
            flex: 1
        }, {
            xtype: 'button',
            reference: 'typeBtn',
            handler: 'onSubmit',
            iconCls: 'x-fa fa-pencil-square-o',
            flex: 0.2
        }]
    }],

    buttons: [{
        iconCls: 'x-fa fa-angle-left',
        scale: 'huge',
        handler: 'goBack'
    }, {
        xtype: 'spacer'
    }, {
        iconCls: 'x-fa fa-home',
        scale: 'huge',
        handler: 'goHome'
    }],


    tools: [
        {
            iconCls: 'x-fa fa-info',
            handler: 'goInfoPage'
        }, {
            iconCls: 'x-fa fa-sign-out',
            handler: 'onSignOut'
        }]

});
