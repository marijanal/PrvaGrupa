Ext.define('MyApp.view.dashboard.Dashboard', {
    extend: 'Ext.panel.Panel',
    xtype: 'dashboard',

    controller: 'dashboard',
    reference: 'dashboard',
    viewModel: 'dashboard',

    bind: {title: '{appLocale.title}'},
    layout: {
        type: 'vbox'
    },

    requires: [
        'MyApp.store.Project',
        'MyApp.view.dashboard.DashboardController'
    ],

    items: [
        {
            xtype: 'panel',
            style: 'margin-top: 10px',
            layout: {
                type: 'vbox',
                align: 'middle',
                pack: 'center'
            },
            items: [{
                xtype: 'button',
                ui: 'squareBtn',
                id: 'btnNewProject',
                reference: 'btnNewProject',
                bind: {text: '{appLocale.btnNewProject}'},
                handler: 'newProject'
            }]
        },

        {
            xtype: 'grid',
            flex: 1,
            store: {
                type: 'project'
            },
            listener: 'dashboard',
            id: 'grid',
            reference: 'grid',
            columns: [{
                bind: {text: '{appLocale.projectName}'},
                style: 'border-bottom: 2px solid black ',
                dataIndex: 'name',
                flex: 1
            },
                {
                    bind: {text: '{appLocale.projectDescription}'},
                    style: 'border-bottom: 2px solid black ',
                    dataIndex: 'description',
                    flex: 1
                }

            ],
            scrollable: 'y'
        }
    ],

    buttons: [{
        iconCls: 'x-fa fa-angle-left',
        scale: 'huge',
        handler: 'goBack'
    }, {
        xtype: 'spacer'
    },
        {
            iconCls: 'x-fa fa-home',
            scale: 'huge',
            handler: 'goHome'
        }],

    tools: [
        {
            iconCls: 'x-fa fa-info',
            handler: 'goInfoPage'
        }, {
        iconCls: 'x-fa fa-sign-out',
        handler: 'onSignOut'
    }]

});
