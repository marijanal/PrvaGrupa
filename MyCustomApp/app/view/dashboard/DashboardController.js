var win;
var alert;
Ext.define('MyApp.view.dashboard.DashboardController', {

    alias: 'controller.dashboard',
    extend: 'Ext.app.ViewController',

    listen: {
        component: {

            '#grid': {
                childtap: function (item, loc) {
                    this.fireEvent('getProject',loc.record.data.name, loc.record.data.id);
                    this.fireEvent('changeTitle', loc.record.data.name);
                    this.redirectTo('dashboard/' + loc.record.data.name);
                }
            },

            '#fromDate' : {
                change: function(obj) {
                    var date = new Date();
                    toDate  = this.getView().lookup('toDate');

                    //provera da li je postavio na dan koji je prosao
                    if(((date - obj.getValue()) / (60 * 60 * 1000)) > 24) {

                        alert = Ext.create({
                            xtype: 'dialog',
                            alwaysOnTop: true,
                            draggable: false,
                            viewModel: 'dashboard',
                            controller: 'dashboard',
                            bind: {
                                title: '{appLocale.warning}',
                                html: '{appLocale.projectStarted}'
                            },

                            buttons: [{
                                bind: {text: '{appLocale.okBtn}'},
                                handler: 'closeAlert'
                            }]
                        }).show();
                    }

                    toDate.setDisabled(false);
                    toDate.setMinDate(obj.getValue());

                }
            }
        }
    },

    addProject: function () {

        var name = this.getView().lookup('projectName').getValue();
        var desc = this.getView().lookup('projectDescription').getValue();
        var fromDate = this.getView().lookup('fromDate').getValue();
        var toDate = this.getView().lookup('toDate').getValue();

        var send = {
            "description": desc,
            "fromDate": fromDate,
            "name": name,
            "toDate": toDate
        };

        if (name != null && (name !== '')) {
            if(fromDate != null && toDate != null) {
                if(this.getView().lookup('toDate').isValid()) {
                    Ext.Ajax.request({

                        defaultHeaders: {
                            'Content-Type': 'application/json'
                        },

                        // url: 'http://192.168.0.15:8080/projects',
                        url: 'http://localhost:8080/projects',
                        method: 'POST',
                        scope: this,
                        params: Ext.encode(send),

                        success: function (response, opts) {
                            Ext.getStore('projects').reload();
                            win.close();
                        },

                        failure: function (response, opts) {
                            alert = Ext.create({
                                xtype: 'dialog',
                                alwaysOnTop: true,
                                draggable: false,
                                viewModel: 'dashboard',
                                controller: 'dashboard',
                                bind: {
                                    title: '{appLocale.notification}',
                                    html: '{appLocale.serverFail}'
                                },

                                buttons: [{
                                    bind: {text: '{appLocale.okBtn}'},
                                    handler: 'closeAlert'
                                }]
                            }).show();
                        }
                    });
                }
                else {
                    alert = Ext.create({
                        xtype: 'dialog',
                        alwaysOnTop: true,
                        draggable: false,
                        viewModel: 'dashboard',
                        controller: 'dashboard',
                        bind: {
                            title: '{appLocale.warning}',
                            html: '{appLocale.toDateFail}'
                        },

                        buttons: [{
                            bind: {text: '{appLocale.okBtn}'},
                            handler: 'closeAlert'
                        }]
                    }).show();
                }
            }
            else {
                this.getView().lookup('fromDate').toggleInvalidCls(true);
                this.getView().lookup('toDate').toggleInvalidCls(true);
                alert = Ext.create({
                    xtype: 'dialog',
                    alwaysOnTop: true,
                    draggable: false,
                    viewModel: 'dashboard',
                    controller: 'dashboard',
                    bind: {
                        title: '{appLocale.notification}',
                        html: '{appLocale.dateFail}'
                    },

                    buttons: [{
                        bind: {text: '{appLocale.okBtn}'},
                        handler: 'closeAlert'
                    }]
                }).show();
            }
        }
        else {
            alert = Ext.create({
                xtype: 'dialog',
                alwaysOnTop: true,
                draggable: false,
                viewModel: 'dashboard',
                controller: 'dashboard',
                bind: {
                    title: '{appLocale.notification}',
                    html: '{appLocale.projectNameFail}'
                },

                buttons: [{
                    bind: {text: '{appLocale.okBtn}'},
                    handler: 'closeAlert'
                }]
            }).show();
        }
    },

    closeAlert: function() {
      alert.close();
    },

    close: function () {
        win.close();
    },

    goBack: function () {
        window.history.back();
    },

    goInfoPage: function () {
        this.redirectTo('infoPage');
    },

    goHome: function () {
        this.redirectTo('dashboard');
    },

    newProject: function () {

        win = Ext.create("Ext.window.Window", {
            controller: 'dashboard',
            viewModel: 'dashboard',
            draggable: false,

            bind: {title: '{appLocale.titleNew}'},

            items: [{
                xtype: 'panel',
                items: [{
                    xtype: 'textfield',
                    allowBlank: false,
                    bind: {label: '{appLocale.projectName}'},
                    reference: 'projectName',
                    required: true
                }, {
                    xtype: 'textfield',
                    allowBlank: false,
                    bind: {label: '{appLocale.projectDescription}'},
                    reference: 'projectDescription',
                    required: true
                }, {
                    xtype: 'datefield',
                    id: 'fromDate',
                    bind: {label: '{appLocale.fromDate}'},
                    reference: 'fromDate',
                    dateFormat: 'd/M/Y',
                    listener: 'dashboard'
                }, {
                    xtype: 'datefield',
                    bind: {label: '{appLocale.toDate}'},
                    reference: 'toDate',
                    dateFormat: 'd/M/Y',
                    disabled: true
                }]
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [{
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    bind: {text: '{appLocale.btnAdd}'},
                    handler: 'addProject',
                    iconCls: 'x-fa fa-file'
                }, {
                    xtype: 'button',
                    bind: {text: '{appLocale.btnClose}'},
                    iconCls: 'x-fa fa-times',
                    handler: 'close'
                }]
            }]
        }).show();

    },

    onSignOut: function () {

        win = Ext.create("Ext.window.Window", {
            controller: 'dashboard',
            viewModel: 'dashboard',
            draggable: false,
            bind: {title: '{appLocale.signOut}'},

            items: [{
                xtype: 'panel',
                bind: {html: '{appLocale.question}'}
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [{
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-check',
                    bind: {text: '{appLocale.yes}'},
                    handler: 'signOut'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-times',
                    bind: {text: '{appLocale.no}'},
                    handler: 'close'
                }]
            }]


        }).show();
    },

    signOut: function () {
        localStorage.removeItem("token");
        window.location.reload();
    }

});
