Ext.define('MyApp.view.dashboard.DashboardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.dashboard',

    data: localStorage.getItem("language") === 'rs' ? {

        appLocale: {
            btnNewProject: "Нови пројекат",
            btnAdd: "Додај",
            btnClose: "Затвори",
            no:"Не",
            projectName: "Име пројекта",
            projectDescription: "Опис пројекта",
            signOut: "Одјави се",
            title: "Сви пројекти",
            titleNew: "Нови пројекат",
            question: "Да ли сте сигурни да желите да се одјавите?",
            yes: "Да",
            toDate: 'до Датума',
            fromDate: 'од Датума',
            notification: 'Обавештење',
            projectNameFail: 'Морате дефинисати име пројекта.',
            okBtn: 'У реду',
            serverFail: 'Серверска грешка, немогуће повезивање са сервером.',
            dateFail: 'Морате унети датум.',
            warning: 'Упозорење',
            projectStarted: 'Додаћете пројекат који је већ почео.',
            toDateFail: 'Погрешан формат крајњег датума. Датум мора бити већи од почетног датума.'
        }

    } : {

        appLocale: {
            btnNewProject: "New project",
            btnAdd: "Add",
            btnClose: "Close",
            no:"No",
            projectName: "Project name",
            projectDescription: "Project description",
            signOut: "Log out",
            title: "All projects",
            titleNew: "New project",
            question:"Are you sure want to log out?",
            yes: "Yes",
            toDate: 'to Date',
            fromDate: 'from Date',
            notification: 'Notification',
            projectNameFail: 'You must define project name.',
            okBtn: 'Ok',
            serverFail: 'Server error, unable to connect to server.',
            dateFail: 'You must enter a date.',
            warning: 'Warning',
            projectStarted: 'You are about to add project that already started.',
            toDateFail: 'Wrong end date format. Must be greater from beginning date.'
        }
    }

});