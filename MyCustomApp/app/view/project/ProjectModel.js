Ext.define('MyApp.view.project.ProjectModel', {

    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.project',

    data: localStorage.getItem("language") === 'rs' ? {
        appLocale: {
            btnNewTask: 'Нови таск',
            change: "Уреди",
            description: 'Опис',
            dateCreated: 'Датум креирања',
            issuerId: 'ИД таска',
            storyPoints: 'Приче',
            summary: 'Резиме',
            question: "Да ли сте сигурни да желите да се одјавите?",
            yes: "Да",
            no:"Не",
            signOut: "Одјави се",
            type: "Тип"
        }

    } : {

        appLocale: {
            btnNewTask: 'New Task',
            change: "Edit",
            description: 'Description',
            dateCreated: 'Date created',
            issuerId: 'Issuer ID',
            storyPoints: 'Story points',
            summary: 'Summary',
            question:"Are you sure want to log out?",
            yes: "Yes",
            no:"No",
            signOut: "Log out",
            type: "Type"
        }
    }
});