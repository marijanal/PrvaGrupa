

Ext.define('MyApp.view.project.Project', {
    extend: 'Ext.panel.Panel',
    xtype: 'project',
    reference: 'project',
    controller: 'project',
    viewModel: 'project',
    id: 'project',

    layout: {
        type: 'vbox'
    },

    requires: [
        'MyApp.store.Tasks',
        'MyApp.view.project.ProjectController'
    ],

    items: [{
        xtype: 'panel',
        style: 'margin-top: 10px',
        layout: {
            type: 'vbox',
            align: 'middle',
            pack: 'center'
        },
        items: [{
            xtype: 'button',
            ui: 'squareBtn',
            id: 'btnNewTask',
            reference: 'btnNewTask',
            bind: {text: '{appLocale.btnNewTask}'},
            handler: 'newTask'
        }]
    }, {
        xtype: 'grid',
        flex: 1,
        columnResize: false,
        store: {
            type: 'tasks'
        },
        id: 'gridTasks',
        reference: 'gridTasks',
        style: 'margin-top: 5%;',
        columns: [{
            flex: 0.1,
            columnMenu: false,
            menuDisabled: true

        }, {

            bind: {text: '{appLocale.description}'},
            flex: 1,
            style: 'border-bottom: 2px solid black ',
            dataIndex: 'description'
        }, {

            bind: {text: '{appLocale.type}'},
            flex: 0.8,
            style: 'border-bottom: 2px solid black ',
            dataIndex: 'ticketType',
            renderer: function (value, metaData, record, row) {

                var cells = row.row.cells;

                value = value.toLowerCase();


                if (value === 'task') {
                    cells[0].setCls('task');
                }
                else if (value === 'bug') {
                    cells[0].setCls('bug');
                }
                else if (value === 'new_feature') {
                    cells[0].setCls('feature')
                }
                else if (value === 'improvement') {
                    cells[0].setCls('improvement')
                }

                return value;
            }
        },{
            bind: {text: '{appLocale.dateCreated}'},
            flex:1,
            style: 'border-bottom: 2px solid black ',
            dataIndex: 'createdDate',
            renderer: Ext.util.Format.dateRenderer('j. M')
        }

        ],
        scrollable: 'y'
    }],

    buttons: [{
        iconCls: 'x-fa fa-angle-left',
        scale: 'huge',
        handler: 'goBack'
    }, {
        xtype: 'spacer'
    }, {
        iconCls: 'x-fa fa-home',
        scale: 'huge',
        handler: 'goHome'
    }],
    tools: [
        {
            iconCls: 'x-fa fa-info',
            handler: 'goInfoPage'
        }, {
            iconCls: 'x-fa fa-sign-out',
            handler: 'onSignOut'
        }]

});
