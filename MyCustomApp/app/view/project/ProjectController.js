
Ext.define('MyApp.view.project.ProjectController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.project',

    listen: {
        component: {
            '#grid': {
                childtap: 'onTap'
             },
             '#gridTasks' : {
                 childtap: 'onTaskTap'
            }
        },
        controller: {
            'dashboard': {
                'changeTitle' : function(title) {
                    this.getView().setTitle(title);
                }
            }
        }

    },

    goInfoPage: function () {
        this.redirectTo('infoPage');
    },

    onTap: function (item, loc) {
        //za menjanje naslova taska
        //this.fireEvent('changeTitle', loc.record.data.name);
        var id = Ext.util.History.getToken().split('/')[1];
        this.redirectTo('dashboard/' + id +'/task/' + loc.record.get('id'));
        // this.fireEvent('sendTaskData', loc.record.getData());
    },

    onTaskTap: function (item, loc) {
        var id = Ext.util.History.getToken().split('/')[1];
        this.redirectTo('dashboard/' + id +'/task/' + loc.record.get('id'));
    },

    close: function () {
        win.close();
    },

    goBack: function () {
        window.history.back();
    },

    goHome: function () {
        this.redirectTo('dashboard');
    },

    newTask: function(){
        var projectId = Ext.util.History.getToken().split('/')[1];

        this.redirectTo('dashboard/'+projectId+'/newTask');

    },

    onSignOut: function () {

        win = Ext.create("Ext.window.Window", {
            controller: 'dashboard',
            viewModel: 'dashboard',
            draggable: false,
            bind:{title: '{appLocale.signOut}'},

            items: [{
                xtype: 'panel',
                bind: {html: '{appLocale.question}'}
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [{
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-check',
                    bind:{text:'{appLocale.yes}'},
                    handler: 'signOut'
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-times',
                    bind:{text:'{appLocale.no}'},
                    handler: 'close'
                }]
            }]


        }).show();
    },

    signOut: function () {
        localStorage.removeItem("token");
        window.location.reload();
    }
});