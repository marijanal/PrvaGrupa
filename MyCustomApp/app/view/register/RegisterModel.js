Ext.define('MyApp.view.register.RegisterModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.register',

    data: localStorage.getItem("language") === 'rs' ? {

        appLocale: {
            fname: "Име",
            lname: "Презиме",
            email: "Електронска пошта",
            username: "Корисничко име",
            password: "Лозинка",
            rpassword: "Понови Лозинку",
            btnBack: "Назад",
            btnRegister: "Потврди",
            naslov: "Регистрација",
            usernameFail: 'Корисничко име је заузето.',
            server: 'Серверска грешка',
            serverFail: 'Серверска грешка са кодом ',
            passwordFail: 'Лозинке нису исте.',
            emailFail: 'Унесите исправну е-маил адресу.',
            form: 'Неисправна форма',
            formFail: 'Молим Вас попуните сва поља.',
            okBtn: 'У РЕДУ'
        }

    } : {

        appLocale: {
            fname: "First name",
            lname: "Last name",
            email: "E-mail",
            username: "Username",
            password: "Password",
            rpassword: "Repeated password",
            btnBack: "Back",
            btnRegister: "Submit",
            naslov: "Register",
            usernameFail: 'Username already taken.',
            server: 'Server error',
            serverFail: 'Server failure with code ',
            passwordFail: 'Passwords are not the same.',
            emailFail: 'Enter the correct email address.',
            form: 'Invalid form',
            formFail: 'Please fill all fields.',
            okBtn: 'OK'
        }
    }


});