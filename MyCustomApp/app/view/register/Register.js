Ext.define('MyApp.view.register.Register', {
    extend: 'Ext.form.Panel',
    xtype: 'register',

    bind:{title: '{appLocale.naslov}'},
    controller: 'register',
    reference: 'register',
    viewModel: 'register',

    layout: {

        type: 'vbox'
    },

    items: [{
        xtype: 'textfield',
        bind: {label: '{appLocale.fname}'},
        name: 'firstName',
        allowBlank: false,
        required: true
    },
        {
            xtype: 'textfield',
            bind: {label: '{appLocale.lname}'},
            name: 'lastName',
            allowBlank: false,
            required: true
        }
        ,
        {
            xtype: 'emailfield',
            reference: 'email',
            bind: {label: '{appLocale.email}'},
            name: 'email',
            allowBlank: false,
            required: true
        }
        ,
        {
            xtype: 'textfield',
            reference: 'username',
            bind: {label: '{appLocale.username}'},
            name: 'username',
            required: true
        }
        ,
        {
            xtype: 'passwordfield',
            reference: 'fieldPassword',
            bind: {label: '{appLocale.password}'},
            name: 'password',
            allowBlank: false,
            required: true
        }
        ,
        {
            xtype: 'passwordfield',
            reference: 'fieldPasswordR',
            bind: {label: '{appLocale.rpassword}'},
            name: 'repeatedPassword',
            allowBlank: false,
            required: true
        }

    ],

    buttons: [
        {
            bind: {text: '{appLocale.btnBack}'},
            handler: 'onBack'

        },
        {
            bind: {text: '{appLocale.btnRegister}'},
            handler: 'onRegister',
            ui: 'squareBtn'
        }
    ],

    tools: [{
        html: '<img src="http://localhost:1841/MyCustomApp/resources/languageIcons/serbia.svg" width="32" height="32">',
        handler: 'onSerbianClick'
    }, {
        xtype: 'spacer',
        width: 10
    }, {
        html: '<img src="http://localhost:1841/MyCustomApp/resources/languageIcons/united-kingdom.svg" width="32" height="32">',
        handler: 'onEnglishClick'
    },

        {
            iconCls: 'x-fa fa-info',
            handler: 'goInfoPage'
        }]

});
