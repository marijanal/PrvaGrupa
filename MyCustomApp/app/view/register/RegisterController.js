/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
var win;
Ext.define('MyApp.view.register.RegisterController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.register',


    goInfoPage: function () {
        this.redirectTo('infoPage');
    },

    onRegister: function () {

        var form = this.getView().getValues();
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        this.getView().validate();
        if(this.getView().isValid()) {
            if (regex.test(form.email)) {

                if (form.password === form.repeatedPassword) {

                    Ext.Ajax.request({

                        defaultHeaders: {
                            'Content-Type': 'application/json'
                        },

                        // url: 'http://192.168.0.15:8080/users/register',
                        url: 'http://localhost:8080/users/register',
                        scope: this,
                        params: Ext.encode(this.getView().getValues()),

                        success: function (response, opts) {
                            var obj = Ext.decode(response.responseText);

                            if (obj) {

                                if (obj.usernameOk && obj.emailOk)
                                    this.redirectTo('login', false);
                                else if (!obj.usernameOk)
                                    Ext.Msg.alert('Notification', 'Username already exists.');
                                else if (!obj.emailOk)
                                    Ext.Msg.alert('Notification', 'Account already exists with this email address.');
                            }
                            else {
                                this.getView().lookup('username').toggleInvalidCls(true);
                                win = Ext.create({
                                    xtype: 'dialog',
                                    draggable: false,
                                    viewModel: 'register',
                                    controller: 'register',
                                    bind: {
                                        title: '{appLocale.username}',
                                        html: '{appLocale.usernameFail}'
                                    },

                                    buttons: [{
                                        bind: {text: '{appLocale.okBtn}'},
                                        handler: 'closeDialog'
                                    }]
                                }).show();
                            }
                        },

                        failure: function (response, opts) {

                            win = Ext.create({
                                xtype: 'dialog',
                                draggable: false,
                                viewModel: 'register',
                                controller: 'register',
                                bind: {
                                    title: '{appLocale.server}',
                                    html: '{appLocale.serverFail}'
                                },

                                buttons: [{
                                    bind: {text: '{appLocale.okBtn}'},
                                    handler: 'closeDialog'
                                }]
                            }).show();
                        }
                    });
                }
                else {
                    this.getView().lookup('fieldPassword').toggleInvalidCls(true);
                    this.getView().lookup('fieldPasswordR').toggleInvalidCls(true);
                    win = Ext.create({
                        xtype: 'dialog',
                        draggable: false,
                        viewModel: 'register',
                        controller: 'register',
                        bind: {
                            title: '{appLocale.password}',
                            html: '{appLocale.passwordFail}'
                        },

                        buttons: [{
                            bind: {text: '{appLocale.okBtn}'},
                            handler: 'closeDialog'
                        }]
                    }).show();
                }
            }
            else {
                this.getView().lookup('email').toggleInvalidCls(true);
                win = Ext.create({
                    xtype: 'dialog',
                    draggable: false,
                    viewModel: 'register',
                    controller: 'register',
                    bind: {
                        title: '{appLocale.email}',
                        html: '{appLocale.emailFail}'
                    },

                    buttons: [{
                        bind: {text: '{appLocale.okBtn}'},
                        handler: 'closeDialog'
                    }]
                }).show();
            }
        }
        else {
            win = Ext.create({
                xtype: 'dialog',
                draggable: false,
                viewModel: 'register',
                controller: 'register',
                bind: {
                    title: '{appLocale.form}',
                    html: '{appLocale.formFail}'
                },

                buttons: [{
                    bind: {text: '{appLocale.okBtn}'},
                    handler: 'closeDialog'
                }]
            }).show();
        }
    },

    onBack: function () {

        this.redirectTo('login');
    },

    onSerbianClick: function() {
        if(!(localStorage.getItem("language") === "rs")) {
            localStorage.setItem("language", "rs");
            window.location.reload();
        }
    },

    onEnglishClick: function() {
        if(!(localStorage.getItem("language") === "en" || localStorage.getItem("language") === "")) {
            localStorage.setItem("language", "en");
            window.location.reload();
        }
    },

    closeDialog: function() {
        win.destroy();
    }

});

