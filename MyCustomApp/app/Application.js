/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('MyApp.Application', {
    extend: 'Ext.app.Application',

    name: 'MyApp',

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    },
    launch: function () {


        // this.redirectTo('login',false);
        //Odkomenatarisi ovo u slucaju da ne radi kod ispod, a kod ispod komentarisi

        var isLoggedIn = localStorage.getItem('token');

        if (!isLoggedIn) {
            this.redirectTo('login', false);
        }
        else {
            var token = Ext.util.History.getToken();
            if (token === 'login' || token === '') {
                this.redirectTo('dashboard', false);
            }
            else {
                this.redirectTo(token, false);
            }
        }

    }
});
