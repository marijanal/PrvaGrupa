Ext.define('MyApp.model.Project', {
    extend: 'MyApp.model.Base',

    fields: [{
        name: 'id'
    }, {
        name: 'name'
    }]
});
