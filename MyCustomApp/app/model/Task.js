Ext.define('MyApp.model.Task', {
    extend: 'MyApp.model.Base',

    fields: [{
        name: 'assigneeId'
    }, {
        name: 'description'
    },{
        name: 'issuerId'
    },{
        name: 'storyPoints'
    },{
        name: 'summary'
    },{
        name: 'type'
    }]
});